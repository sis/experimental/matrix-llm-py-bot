FROM ubuntu:jammy as build

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
      python3 build-essential python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --root /dist "llama-cpp-python<=0.1.48"

# ---

FROM ubuntu:jammy

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
      python3 python3-matrix-nio curl \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /dist/usr /usr
COPY ./matrix-llm-bot.py /usr/local/bin/

#RUN useradd -M --system bot && mkdir /etc/matrix-llm-bot
#USER bot

ENTRYPOINT ["/usr/bin/python3", "/usr/local/bin/matrix-llm-bot.py", "-v", "/etc/matrix-llm-bot/config.json"]
#ENTRYPOINT [ "/bin/bash", "-c" ]
