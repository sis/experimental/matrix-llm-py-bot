#!/usr/bin/env python3

# pylint: disable=logging-fstring-interpolation
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=too-few-public-methods

import asyncio
import os
import sys
import traceback
import logging
import json
import time
import threading
from typing import Any, Optional
from dataclasses import dataclass, asdict


import nio
from llama_cpp import Llama

# This file is for restoring login details after closing the program, so you
# can preserve your device ID. If @alice logged in every time instead, @bob
# would have to re-verify. See the restoring login example for more into.
SESSION_DETAILS_FILE = "credentials.json"


@dataclass
class Config:
    accepted_users: set[str]
    accepted_homeservers: set[str]
    homeserver: str
    user_id: str
    user_password: str
    model: str
    encryption_enabled: bool = True
    store_path: Optional[str] = None
    device_name: str = "MatrixLlmBot"
    device_id: str = "matrix-llm-bot"
    welcome_message: str = "🤖 Hello human!"

    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> "Config":
        return cls(
            accepted_users=set(data.pop("accepted_users")),
            accepted_homeservers=set(data.pop("accepted_homeservers")),
            **data,
        )


@dataclass
class Session:
    access_token: str
    user_id: str
    device_id: str

    def save(self, path: str) -> None:
        with open(path, "w", encoding="utf-8") as f:
            json.dump(asdict(self), f)


async def main(config_path: str) -> None:
    try:
        with open(config_path, "rb") as stream:
            config = Config.from_dict(json.load(stream))
        bot = MatrixLlmBot(config)
        if not bot.load_session():
            await bot.login(config.user_password, config.device_name)
        logging.info(f"connecting to {config.homeserver}")
        await bot.sync_forever(timeout=30000, full_state=config.store_path is not None)
        await bot.close()
    except Exception:  # pylint: disable=broad-exception-caught
        logging.error(traceback.format_exc().strip())
        sys.exit(1)
    except KeyboardInterrupt:
        sys.exit(0)


class AutoTrustAsyncClient(nio.AsyncClient):
    def __init__(
        self,
        homeserver: str,
        user_id: str,
        device_id: str,
        store_path: Optional[str] = None,
        encryption_enabled=True,
    ):
        super().__init__(
            homeserver=homeserver,
            user=user_id,
            device_id=device_id,
            store_path=store_path,
            config=nio.AsyncClientConfig(
                max_limit_exceeded=0,
                max_timeouts=0,
                store_sync_tokens=store_path is not None,
                encryption_enabled=encryption_enabled,
            ),
        )
        self.user_id = user_id
        self.store_path = store_path
        self._initial_sync_done = False

        self.add_response_callback(self._on_error, nio.SyncError)
        self.add_response_callback(self._on_sync, nio.SyncResponse)
        self.add_event_callback(self._on_megolm_event, nio.MegolmEvent)
        self.add_event_callback(self._on_room_event, nio.events.room_events.Event)
        self.add_event_callback(self._on_member_event, nio.RoomMemberEvent)

    async def login(self, password: str, device_name: str) -> None:
        login_response = await super().login(
            password=password,
            device_name=device_name,
        )
        if isinstance(login_response, nio.LoginError):
            raise RuntimeError(f"💥 Failed to login: {login_response.message}")
        session_path = os.path.join(self.store_path, SESSION_DETAILS_FILE)
        Session(
            access_token=login_response.access_token,
            device_id=login_response.device_id,
            user_id=login_response.user_id,
        ).save(session_path)
        logging.info(f"Logged in as {self.user_id}")

    def load_session(self) -> bool:
        session_path = os.path.join(self.store_path, SESSION_DETAILS_FILE)
        try:
            with open(session_path, "rb") as f:
                session = Session(**json.load(f))
            self.restore_login(
                user_id=session.user_id,
                device_id=session.device_id,
                access_token=session.access_token,
            )

            # This loads our verified/blacklisted devices and our keys
            self.load_store()
            logging.info(
                f"Logged in using stored credentials: {self.user_id} on {self.device_id}"
            )
        except (FileNotFoundError, IOError, json.JSONDecodeError) as err:
            logging.info(f"Couldn't load session from file: {err}")
            return False
        return True

    async def _on_sync(self, _response):
        if not self._initial_sync_done:
            self._initial_sync_done = True
            for room_id in self.rooms:
                logging.info(f"joined room {room_id}")
            logging.info("initial sync done, ready for work")

    async def _on_megolm_event(self, room, event):
        logging.warning(f"Decryption failed: {event}")
        if self.config.encryption_enabled:
            msg = "My encryption keys got lost in my state. Sorry!"
        else:
            msg = "I dont like encrypted rooms!"
        if room.room_id in self.rooms:
            await self.room_send(
                room_id=room.room_id,
                message_type="m.room.message",
                content={
                    "msgtype": "m.text",
                    "body": msg + " I will invite you to an unencrypted room",
                },
                ignore_unverified_devices=True,
            )
        await self.room_leave(room.room_id)
        await self.room_forget(room.room_id)
        logging.warning("Creating new room")
        await self.room_create(is_direct=True, invite=(event.sender,))

    async def _on_member_event(self, room, event):
        logging.info(f"Member room={room.room_id} event: {event.membership}")
        if (
            room.member_count == 1
            and room.invited_count == 0
            and room.room_id in self.rooms
            and event.membership == "leave"
        ):
            logging.info(f"All humans gone. Leaving room {room.room_id}")
            await self.room_leave(room.room_id)
            await self.room_forget(room.room_id)

    async def _on_error(self, response):
        if self:
            await self.close()
        raise RuntimeError(response)

    async def _on_room_event(self, room, source):
        logging.debug(f"Event: room={room}, src={source}")

    def verify_room_devices(self, room_id: str) -> None:
        if self.room_contains_unverified(room_id):
            for device_dict in self.room_devices(room_id).values():
                for device in device_dict.values():
                    logging.info(f"Verifying device={device}")
                    self.verify_device(device)


class MatrixLlmBot(AutoTrustAsyncClient):
    def __init__(self, config: Config):
        self.bot_config = config
        self.model = Llama(model_path=config.model)
        self._last_event_timestamp = time.time() * 1000
        self._welcome_msg_lock = threading.Lock()
        self._processed_welcome_msg_events = set()
        super().__init__(
            config.homeserver,
            user_id=config.user_id,
            device_id=config.device_id,
            store_path=config.store_path,
            encryption_enabled=config.encryption_enabled,
        )
        self.add_event_callback(self._on_invite, nio.InviteMemberEvent)
        self.add_event_callback(self._on_message, nio.RoomMessageText)

    async def _on_invite(self, room, event):
        if (
            not event.sender in self.bot_config.accepted_users
            and not event.sender.split(":", 1)[-1]
            in self.bot_config.accepted_homeservers
        ):
            logging.info(f"invite from {event.sender} to {room.room_id} rejected")
            await self.room_leave(room.room_id)
            return
        logging.info(f"invite from {event.sender} to {room.room_id} accepted")
        await self.join(room.room_id)
        await self.sync()
        self.verify_room_devices(room.room_id)
        # hack to prevent sending the msg twice:
        with self._welcome_msg_lock:
            if room.room_id in self._processed_welcome_msg_events:
                return
            self._processed_welcome_msg_events.add(room.room_id)
        await self.room_send(
            room_id=room.room_id,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": self.bot_config.welcome_message},
        )

    async def _on_message(self, room, event):
        await self.update_receipt_marker(room.room_id, event.event_id)
        if event.sender == self.user_id:
            return
        if event.server_timestamp <= self._last_event_timestamp:
            return
        self._last_event_timestamp = event.server_timestamp
        logging.info(
            f"Got msg: room={room.room_id}, sender={event.sender}, body={event.body}"
        )
        await self.room_typing(room.room_id, True)
        self.verify_room_devices(room.room_id)
        logging.debug(
            f"Room contains unverified devices: {self.room_contains_unverified(room.room_id)}, devices={self.room_devices(room.room_id)}"
        )

        n_attempts = 3
        for _ in range(n_attempts):
            try:
                output = self.model(f"Q: {event.body} A: ", stop=["Q:", "### Human:"])
                response = output["choices"][0]["text"]
                break
            except Exception as exc:  # pylint: disable=broad-exception-caught
                response = "💥 Model crashed"
                logging.error(f"Model crash: {exc}")
        await self.room_send(
            room_id=room.room_id,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": response},
        )

        await self.room_typing(room.room_id, False)


class EmojiLevelFormatter(logging.Formatter):
    emojis = {
        logging.DEBUG: "🔬",
        logging.INFO: "ℹ️",
        logging.WARNING: "⚠️",
        logging.ERROR: "💥",
        logging.CRITICAL: "☢️",
    }

    def format(self, record: logging.LogRecord) -> str:
        record.levelname = self.emojis.get(record.levelno, record.levelname)
        return logging.Formatter.format(self, record)


def setup_logging(log_level: int = logging.INFO) -> None:
    logger = logging.getLogger()
    logger.setLevel(log_level)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(EmojiLevelFormatter("%(levelname)s  %(message)s"))
    logger.addHandler(console_handler)


if __name__ == "__main__":
    import argparse

    _parser = argparse.ArgumentParser()

    _parser.add_argument(
        "config",
        help="Config file",
    )
    _parser.add_argument(
        "-v",
        dest="log_level",
        action="count",
        default=0,
        help="Verbose",
    )
    _args = _parser.parse_args()
    _verbosity = {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}
    setup_logging(_verbosity[min(_args.log_level, 2)])
    try:
        asyncio.run(main(_args.config))
    except KeyboardInterrupt:
        pass
