# matrix LLM bot

Proof of concept to connect [Vicuna](https://vicuna.lmsys.org/) (a large language model) to matrix.

## Usage

* Get `ggml-vicuna-13b-4bit.bin` from
https://huggingface.co/eachadea/legacy-ggml-vicuna-13b-4bit
* Create a matrix account (for the bot)
* Prepare a config `./private/config.json`
  ```json
  {
    "accepted_users": [],
    "accepted_homeservers": ["staffchat.ethz.ch"],
    "homeserver": "https://matrix.org",
    "user_id": "@...:matrix.org",
    "user_password": "🔑",
    "model": "/var/lib/matrix-llm-bot/ggml-vicuna-13b-4bit.bin",
    "store_path": "/var/cache/matrix-llm-bot",
    "device_id": "matrix-llm-bot",
    "welcome_message": "🤖 Hello human! I am a 🦙 [Vicuna](https://vicuna.lmsys.org/) model"
  }
  ```

* Run the bot

    - Using docker 🐋 / podman 🦭:
    ```sh
    docker run -it --rm -v ./private/config.json:/etc/matrix-llm-bot/config.json -v ./ggml-vicuna-13b-4bit.bin:/var/lib/matrix-llm-bot/ggml-vicuna-13b-4bit.bin registry.ethz.ch/sis/experimental/matrix-llm-py-bot/matrix-llm-py-bot
    ```
    - On the local machine:
    ```sh
    pip install -e .
    ./matrix-llm-bot.py private/config.json
    ```

After that, start a chat with the matrix bot / create a matrix room
and invite your bot. It
should automatically join the room and respond to your messages.

## Todo list

* [ ] Load balancing / load limiting
* [ ] Make the call to the llm `async` / improve scheduling / notify
      human users about message queue length
* [ ] Make use of LLM context (right now: no context - the bot ignores
      the chat history)
